class HomeController < ApplicationController
  def index
  end

  protected
  def authenticate_user!
    if user_signed_in?
      super
    else
      redirect_to new_user_session_path, :notice => 'Please login'
    end
  end
end